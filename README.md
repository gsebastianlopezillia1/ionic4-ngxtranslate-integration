# ionic4-ngxTranslate-integration

Blank project with @ngx-translate/core@9.1.1 integrated and working on two languages (es-en). I´ve used the 9.1.1 ngx-translate version to avoid some issues that appears on the 10^ versions. I expect this be usefully to you :)

READ HOW TO USE THE TRANSLATE-SERVICE IN https://github.com/ngx-translate/core

Ionic:

   ionic (Ionic CLI)  : 4.1.0 (C:\Users\User\AppData\Roaming\npm\node_modules\ionic)
   Ionic Framework    : ionic-angular 3.9.2
   @ionic/app-scripts : 3.2.0

Cordova:

   cordova (Cordova CLI) : 8.0.0
   Cordova Platforms     : android 7.0.0
   Cordova Plugins       : cordova-plugin-ionic-keyboard 2.1.3, cordova-plugin-ionic-webview 2.2.0, (and 4 other plugins)

System:

   NodeJS : v10.8.0 (C:\Program Files\nodejs\node.exe)
   npm    : 6.2.0
   OS     : Windows 10