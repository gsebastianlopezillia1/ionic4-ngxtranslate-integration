import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslateService } from "@ngx-translate/core"; //https://github.com/ngx-translate/core


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  lang: boolean = true

  constructor(public navCtrl: NavController,
    private translate: TranslateService) {
      /**
       * READ HOW TO USE THE TRANSLATE-SERVICE IN https://github.com/ngx-translate/core
       */
      
  }

  updateLang(){
    if(this.lang){
      this.translate.use('es')
    }else{
      this.translate.use('en')
    }
  }



}
